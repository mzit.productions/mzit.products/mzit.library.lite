﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library
{
    public partial class addBookForm : Form
    {
        public addBookForm()
        {
            InitializeComponent();
        }

        private void addBookForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(FormMain.conStr);
            bool conD = textBox_bookName.Text != "" &&
                textBox_writerName.Text != "" &&
                textBox_author.Text != "" &&
                textBox_isbn.Text != "";
            if (conD)
            {

                conn.Open();
                // prepare command string
                string insertString = @"insert into books
                        (bookSubject, writerName, author, ISBN)
                        Values
                             ('" + textBox_bookName.Text + "'," +
                            " '" + textBox_writerName.Text + "'," +
                            " '" + textBox_author.Text + "'," +
                            " '" + textBox_isbn.Text + "')";

                // 1. Instantiate a new command with a query and connection
                SqlCommand cmd = new SqlCommand(insertString, conn);

                // 2. Call ExecuteNonQuery to send command
                cmd.ExecuteNonQuery();

                textBox_bookName.Clear();
                textBox_writerName.Clear();
                textBox_author.Clear();
                textBox_isbn.Clear();
                MessageBox.Show("کتاب شما با موفقیت ثبت شد !");
                conn.Close();
            }
            else
            {
                MessageBox.Show("تمام فیلد ها را پر کنید !!!");
            }
        }
    }
}
