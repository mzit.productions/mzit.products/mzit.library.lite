﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library
{
    public partial class addToUserForm : Form
    {
        public addToUserForm()
        {
            InitializeComponent();
        }

        private void addToUserForm_Load(object sender, EventArgs e)
        {
            loadUsers();
            loadBooks();
        }
        List<string> userIDList;
        void loadUsers()
        {
            userIDList = new List<string>();
            listBox1.Items.Clear();
            SqlConnection conn = new SqlConnection(FormMain.conStr);

            SqlDataReader rdr = null;
            try
            {
                // 2. Open the connection
                conn.Open();

                // 3. Pass the connection to a command object
                SqlCommand cmd = new SqlCommand("select * from users", conn);

                // 4. Use the connection
                // get query results
                rdr = cmd.ExecuteReader();

                // print the CustomerID of each record
                while (rdr.Read())
                {
                    //Console.WriteLine(rdr[0]);
                    listBox1.Items.Add(rdr[0] + " - " +
                        rdr[1] + " - " +
                        rdr[2] + " - " +
                        rdr[3] + " - " +
                        rdr[4] + " - ");
                    userIDList.Add(rdr[0].ToString());
                }
            }
            finally
            {
                // close the reader
                if (rdr != null)
                {
                    rdr.Close();
                }

                // 5. Close the connection
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }


        List<string> bookIDList;
        void loadBooks()
        {
            bookIDList = new List<string>();
            SqlConnection conn = new SqlConnection(FormMain.conStr);

            SqlDataReader rdr = null;
            try
            {
                // 2. Open the connection
                conn.Open();

                // 3. Pass the connection to a command object
                SqlCommand cmd = new SqlCommand("select * from books", conn);

                // 4. Use the connection
                // get query results
                rdr = cmd.ExecuteReader();

                // print the CustomerID of each record
                while (rdr.Read())
                {
                    //Console.WriteLine(rdr[0]);
                    listBox2.Items.Add(rdr[0] + " - " +
                        rdr[1] + " - " +
                        rdr[2] + " - " +
                        rdr[3] + " - " +
                        rdr[4] + " - ");
                    bookIDList.Add(rdr[0].ToString());
                }
            }
            finally
            {
                // close the reader
                if (rdr != null)
                {
                    rdr.Close();
                }

                // 5. Close the connection
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1 &&
                listBox2.SelectedIndex != -1)
            {
                string selectedUserID = userIDList[listBox1.SelectedIndex];
                string selectedBookID = bookIDList[listBox2.SelectedIndex];
                SqlConnection conn = new SqlConnection(FormMain.conStr);

                try
                {
                    // 2. Open the connection
                    conn.Open();
                    // 3. Pass the connection to a command object
                    SqlCommand cmd = new SqlCommand("UPDATE users SET " +
                        "BorrowDate = '" + DateTime.Now.ToShortDateString() +
                        "', borrowedBookID = " + selectedBookID + " WHERE userID = " +
                        selectedUserID, conn);

                    // 4. Use the connection
                    // get query results
                    cmd.ExecuteReader();
                    loadUsers();
                    MessageBox.Show("ثبت شد !");
                }
                catch
                {
                    MessageBox.Show("خطا");
                }
                finally
                {
                    // 5. Close the connection
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            else
            {
                MessageBox.Show("تمام فیلد ها را پر کنید !!!");
            }
        }
    }
}
