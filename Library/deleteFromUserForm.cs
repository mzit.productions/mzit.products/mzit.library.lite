﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library
{
    public partial class deleteFromUserForm : Form
    {
        public deleteFromUserForm()
        {
            InitializeComponent();
        }
        List<string> userIDList;
        private void deleteFromUserForm_Load(object sender, EventArgs e)
        {
            userIDList = new List<string>();
            SqlConnection conn = new SqlConnection(FormMain.conStr);

            SqlDataReader rdr = null;
            try
            {
                // 2. Open the connection
                conn.Open();

                // 3. Pass the connection to a command object
                SqlCommand cmd = new SqlCommand("SELECT u.userID, u.fName, u.lName," +
                    " u.BorrowDate, b.bookSubject, b.writerName, b.author, b.ISBN " +
                    "FROM users u INNER JOIN books b " +
                    "ON u.borrowedBookID = b.bookID", conn);

                // 4. Use the connection
                // get query results
                rdr = cmd.ExecuteReader();

                // print the CustomerID of each record
                while (rdr.Read())
                {
                    //Console.WriteLine(rdr[0]);
                    listBox1.Items.Add(rdr[0] + " - " +
                        rdr[1] + " - " +
                        rdr[2] + " - " +
                        rdr[3] + " - " +
                        rdr[4] + " - " +
                        rdr[5] + " - " +
                        rdr[6] + " - " +
                        rdr[7]);
                    userIDList.Add(rdr[0].ToString());
                }
            }
            finally
            {
                // close the reader
                if (rdr != null)
                {
                    rdr.Close();
                }

                // 5. Close the connection
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                string delID = userIDList[listBox1.SelectedIndex];
                SqlConnection conn = new SqlConnection(FormMain.conStr);

                try
                {
                    // 2. Open the connection
                    conn.Open();
                    // 3. Pass the connection to a command object
                    SqlCommand cmd = new SqlCommand("UPDATE users SET " +
                        "BorrowDate = NULL, borrowedBookID = NULL WHERE userID = " + delID, conn);

                    // 4. Use the connection
                    // get query results
                    cmd.ExecuteReader();
                    listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                    MessageBox.Show("حذف شد !");
                }
                catch
                {
                    MessageBox.Show("خطا");
                }
                finally
                {
                    // 5. Close the connection
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            else
            {
                MessageBox.Show("لطفا یک مورد را انتخاب کنید !");
            }
        }
    }
}
