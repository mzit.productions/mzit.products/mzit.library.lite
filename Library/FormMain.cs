﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        public static string conStr = @"Data Source=.\SQLEXPRESS;Initial Catalog=LibraryDB;User ID=sa;Password=sa_1234";

        private void FormMain_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(FormMain.conStr);

            SqlDataReader rdr = null;
            try
            {
                // 2. Open the connection
                conn.Open();

                // 3. Pass the connection to a command object
                SqlCommand cmd = new SqlCommand("select * from books", conn);

                // 4. Use the connection
                // get query results
                rdr = cmd.ExecuteReader();

                // print the CustomerID of each record
                while (rdr.Read())
                {
                    //Console.WriteLine(rdr[0]);
                    listBox1.Items.Add(rdr[1]);
                }
            }
            finally
            {
                // close the reader
                if (rdr != null)
                {
                    rdr.Close();
                }

                // 5. Close the connection
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void addBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addBookForm t = new addBookForm();
            t.Show();
        }

        private void addUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addUserForm t = new addUserForm();
            t.Show();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void deleteUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            userDeleteFrom t = new userDeleteFrom();
            t.Show();
        }

        private void deleteBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            deleteBookForm t = new deleteBookForm();
            t.Show();
        }

        private void deleteFromUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            deleteFromUserForm t = new deleteFromUserForm();
            t.Show();
        }

        private void addToUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addToUserForm t = new addToUserForm();
            t.Show();
        }
    }
}
