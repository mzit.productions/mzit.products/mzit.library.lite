﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library
{
    public partial class addUserForm : Form
    {
        public addUserForm()
        {
            InitializeComponent();
        }

        private void addUserForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(FormMain.conStr);
            bool conD = textBox_firstName.Text != "" &&
                textBox_lastName.Text != "";
            if (conD)
            {

                conn.Open();
                // prepare command string
                string insertString = @"insert into users
                        (fName, lName, BorrowDate, borrowedBookID)
                        Values
                             ('" + textBox_firstName.Text + "'," +
                            " '" + textBox_lastName.Text + "'," +
                            " ''," +
                            " '')";

                // 1. Instantiate a new command with a query and connection
                SqlCommand cmd = new SqlCommand(insertString, conn);

                // 2. Call ExecuteNonQuery to send command
                cmd.ExecuteNonQuery();

                textBox_firstName.Clear();
                textBox_lastName.Clear();
                MessageBox.Show("کاربر شما با موفقیت ثبت شد !");
                conn.Close();
            }
            else
            {
                MessageBox.Show("تمام فیلد ها را پر کنید !!!");
            }
        }
    }
}
